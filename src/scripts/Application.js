import BasicGame	   from 'odie/BasicGame';
import Camera from 'odie/components/view3d/Camera';
import View3d from 'odie/components/view3d/View3d';
import BasicEntity from 'odie/BasicEntity';
import BasicMaterial from './BasicMaterial';
import Primitive from './Primitive';
import * as glm from 'gl-matrix';

export default class App extends BasicGame
{
    constructor()
    {
        const view = new PIXI.Container();

        super(view);
        this.view = view;
        this.camera = new Camera();
        // console.log(this.camera.uniforms.uniforms.view);
        // this.camera.transform.position.z = -10;
        glm.mat4.lookAt(this.camera.transform.worldTransform, [0, 0, 10], [0, 0, 0], [0, 1, 0]);
        // glm.mat4.lookAt(this.camera.uniforms.uniforms.view, [0, 0, -20], [0, 0, 0], [0, 1, 0]);

        this.system.view3dSystem.setCamera(this.camera);
        const prim = Primitive.cube();

        const geometry = new PIXI.Geometry();

        geometry.addAttribute('aVertexPosition', prim.positions, 3);
        geometry.addAttribute('aUv', prim.uvs, 2);
        geometry.addIndex(prim.indices);

        const cube = new BasicEntity();

        cube.add(View3d, {
            geometry,
            material: new BasicMaterial({
                color: [1, 1, 1],
            }),
        });
        cube.transform.position.x = 1;
        this.addChild(cube);
        cube.addScript({
            update()
            {
                // console.log('update');
            },
        });
    }
    update()
    {
        super.update();
    }
}

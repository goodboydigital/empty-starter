import * as PIXI from 'pixi.js';
import * as glm from 'gl-matrix';
import Application from './Application';
import BasicMaterial from './BasicMaterial';
import PhongMaterial from './PhongMaterial';
import Primitive from './Primitive';
import Camera from './Camera';
import OrbitalCameraControl from './Orbital';
import objParser from './middlewares/objParser';
import object3d from './object3d';

const app = new PIXI.Application({
    autoStart: true,
    width: window.innerWidth,
    height: window.innerHeight,
});

document.body.appendChild(app.view);

const appOdie = new Application();
//

// app.stage.addChild(appOdie.view);
// // console.log(appOdie.view);
//
// // console.log(appOdie.view);

const camera = new Camera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
const controls = new OrbitalCameraControl(camera, 20, window);
const prim = Primitive.cube();

// const geometry = new PIXI.Geometry();
const geometry = objParser(object3d);

// console.log(geo);
//
// geometry.addAttribute('aVertexPosition', prim.positions, 3);
// geometry.addAttribute('aUv', prim.uvs, 2);
// geometry.addAttribute('aNormal', prim.normals, 3);
// geometry.addIndex(prim.indices);

const texture = new PIXI.Texture.from('assets/image/color-main-tower.jpg');
const shader = new PhongMaterial({
    // color: [0.0, 1.0, 0.0],
    shininess: 300,
    // specular: [1.0, 1.0, 1.0],
    // map: texture,
    // opacity: 0.5,
    // fog: true,
    uniforms: {
        uDirectionLightDirection: [-5, 0, 1, 5, 10, 1, 0, 5, 1],
        uDirectionLightColor: [1, 0, 0, 0, 0, 1, 0, 1, 0],
        uAmbiantLight: [0.1, 0.1, 0.1],
    },
});

const state =  new PIXI.State();

state.depthTest = true;
state.blend = false;
const mesh = new PIXI.RawMesh(geometry, shader, state);

mesh.shader.uniforms.projection = camera.projection;
mesh.shader.uniforms.view = camera.view;
const modelMatrix = glm.mat4.create();
const identity = glm.mat4.create();
let tick = 0;

const normalMatrix4 = glm.mat4.create();
const normalMatrix3 = glm.mat3.create();
const modelViewMatrix = glm.mat4.create();
const inverseModelViewMatrix = glm.mat4.create();

app.ticker.add(() =>
{
    tick += 0.005;
    controls.update();
    // glm.mat4.rotate(modelMatrix, identity, tick, [0, 1, 0]);
    // glm.mat4.rotate(modelMatrix, modelMatrix, 0.4, [1, 0, 0]);

    glm.mat4.identity(modelViewMatrix);
    glm.mat4.multiply(modelViewMatrix, camera.view, modelMatrix);
    glm.mat4.invert(inverseModelViewMatrix, modelViewMatrix);
    glm.mat4.transpose(normalMatrix4, inverseModelViewMatrix);

    appOdie.update();
    mesh.shader.uniforms.model = modelMatrix;
    mesh.shader.uniforms.normalMatrix = glm.mat3.fromMat4(normalMatrix3, normalMatrix4);

    mesh.shader.uniforms.uDensity = 0.05;
    mesh.shader.uniforms.uGradient = 2.0;
    mesh.shader.uniforms.uFogColor = [0.0, 0.0, 0.0];
});

app.stage.addChild(mesh);

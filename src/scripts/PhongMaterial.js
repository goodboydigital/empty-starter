import * as PIXI from 'pixi.js';
import vertexSource from './shaders/phong.vert';
import fragmentSource from './shaders/phong.frag';

function filterEmptyLine(string)
{
    return string !== '';
}

export default class BasicMaterial extends PIXI.Shader
{
    constructor(options = {})
    {
        const vs = vertexSource;
        const fs = fragmentSource;

        const definesVs = [
            options.color ? '#define COLOR' : '',
            options.map ? '#define MAP' : '',
            options.fog ? '#define FOG' : '',
        ].filter(filterEmptyLine).join('\n');

        const definesFs = [
            options.color ? '#define COLOR' : '',
            options.map ? '#define MAP' : '',
            options.fog ? '#define FOG' : '',
        ].filter(filterEmptyLine).join('\n');

        let vertex = `${definesVs}\n${vs}`;
        let fragment = `${definesFs}\n${fs}`;

        if (!options.vertex) options.vertex = {};

        vertex = vertex.replace(/#HOOK_VERTEX_START/g, options.vertex.start || '');
        vertex = vertex.replace(/#HOOK_VERTEX_MAIN/g, options.vertex.main || '');
        vertex = vertex.replace(/#HOOK_VERTEX_END/g, options.vertex.end || '');

        if (!options.fragment) options.fragment = {};

        fragment = fragment.replace(/#HOOK_FRAGMENT_START/g, options.fragment.start || '');
        fragment = fragment.replace(/#HOOK_FRAGMENT_MAIN/g, options.fragment.main || '');
        fragment = fragment.replace(/#HOOK_FRAGMENT_END/g, options.fragment.end || '');

        const uniforms  = Object.assign({}, {
            uColor: options.color || [0, 0, 0],
            uEmissive: options.emissive || [0, 0, 0],
            uSpecular: options.specular || [0.0, 0.0, 0.0],
            uShininess: options.shininess || 30,
            uMap: options.map || PIXI.Texture.EMPTY,
            uOpacity: options.opacity !== undefined ? options.opacity : 1,
        }, options.uniforms);

        super(PIXI.Program.from(vertex, fragment), uniforms);
    }
}

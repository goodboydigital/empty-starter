// basic.frag
precision highp float;

uniform float uOpacity;
uniform float uShininess;
uniform vec3 uAmbiantLight;

uniform vec3 uEmissive;
uniform vec3 uSpecular;
uniform vec3 uDirectionLightDirection[3];
uniform vec3 uDirectionLightColor[3];
varying vec3 vNormal;
varying vec3 vPosition;

#ifdef COLOR
uniform vec3 uColor;
#endif

#ifdef MAP
  varying vec2 vUv;
  uniform sampler2D uMap;
#endif

#ifdef FOG
  varying float vFogFactor;
  uniform vec3 uFogColor;
#endif



float diffuse(vec3 N, vec3 L) {
	return max(dot(N, normalize(L)), 0.0);
}


vec3 diffuseLighting(vec3 N, vec3 L, vec3 C) {
	return diffuse(N, L) * C;
}

vec3 specularLighting(vec3 N, vec3 L, vec3 V) {
  float lambertian = max(dot(N, L), 0.0);
  float specular = 0.0;
  if(lambertian > 0.0) {
    vec3 R = reflect(-L, N);      // Reflected light vector
    vec3 V = normalize(-vPosition); // Vector to viewer
    float specAngle = max(dot(R, V), 0.0);
    specular = pow(specAngle, uShininess);
  }
  return specular * uSpecular;
}

// vec3 pointLighting()


void main() {
  vec4 finalColor = vec4(uEmissive, 1.0);
  vec4 diffuseColor = vec4(1., 1., 1., uOpacity);

  #ifdef COLOR
    diffuseColor.rgb *= uColor;
  #endif

  #ifdef MAP
    diffuseColor *= texture2D(uMap, vUv);
  #endif

  vec3 N = normalize(vNormal);
  vec3 V = normalize(-vPosition);
  vec3 lighting = vec3(0.0);
  for(int i = 0; i < 3; i++)
  {

    vec3 L = normalize(uDirectionLightDirection[i] - vPosition);
    // vec3 L = normalize(vec3(0.0,10.0,1.0) - vPosition);
    lighting +=   uAmbiantLight + diffuseLighting(N,L,uDirectionLightColor[i]) + specularLighting(N,L,V);

  }


  finalColor.rgb = diffuseColor.rgb * lighting;
  gl_FragColor = vec4(finalColor.rgb, finalColor.a);

  #ifdef FOG
    gl_FragColor.rgb = mix(uFogColor, gl_FragColor.rgb, vFogFactor);
  #endif


}

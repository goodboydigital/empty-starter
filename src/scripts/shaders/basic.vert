// basic.vert
precision highp float;
attribute vec3 aPosition;
attribute vec2 aUv;

uniform mat4 projection;
uniform mat4 view;

#ifdef MAP
	varying vec2 vUv;
	uniform sampler2D uMap;
#endif

#ifdef FOG
  uniform float uDensity;
  uniform float uGradient;
  varying float vFogFactor;
#endif

#HOOK_VERTEX_START

void main() {

	#ifdef MAP
		vUv = aUv;
	#endif

	vec3 transformed = aPosition;

	#HOOK_VERTEX_MAIN

	vec4 worldPosition = projection * view * vec4(transformed, 1.0);
	gl_Position = worldPosition;

	#ifdef FOG
	 float dist = length(worldPosition.xyz);
	 vFogFactor = exp(-pow(dist * uDensity, uGradient));
	 vFogFactor = clamp(vFogFactor, 0.0, 1.0);
 #endif

	#HOOK_VERTEX_END

}

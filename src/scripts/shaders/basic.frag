// basic.frag
precision highp float;

uniform float uOpacity;

#ifdef COLOR
uniform vec3 uColor;
#endif

#ifdef MAP
  varying vec2 vUv;
  uniform sampler2D uMap;
#endif

#ifdef FOG
  varying float vFogFactor;
  uniform vec3 uFogColor;
#endif

#HOOK_FRAGMENT_START

void main() {
  vec4 finalColor = vec4(1.0, 1.0, 1.0, uOpacity);

  #ifdef COLOR
    finalColor.rgb *= uColor;
  #endif

  #ifdef MAP
    finalColor *= texture2D(uMap, vUv);
  #endif

  #HOOK_FRAGMENT_MAIN

  gl_FragColor = vec4(finalColor.rgb, finalColor.a);
  gl_FragColor *= uOpacity;

  #HOOK_FRAGMENT_END

  
  #ifdef FOG
    gl_FragColor.rgb = mix(uFogColor, gl_FragColor.rgb, vFogFactor);
  #endif

}

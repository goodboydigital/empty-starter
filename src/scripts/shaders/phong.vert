// basic.vert
precision highp float;
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat3 normalMatrix;

varying vec3 vNormal;
varying vec3 vPosition;
#ifdef MAP
	varying vec2 vUv;
	uniform sampler2D uMap;
#endif

#ifdef FOG
  uniform float uDensity;
  uniform float uGradient;
  varying float vFogFactor;
#endif

#HOOK_VERTEX_START

void main() {

	vNormal = normalize(normalMatrix * aNormal);
	#ifdef MAP
		vUv = aUv;
	#endif

	vec3 transformed = aPosition;

	#HOOK_VERTEX_MAIN

	vec4 worldPosition = projection * view * model * vec4(transformed, 1.0);
	vec4 worldPos =  model * vec4(transformed, 1.0);
	vPosition =  worldPos.xyz;
	gl_Position = worldPosition;

	#ifdef FOG
	 float dist = length(worldPosition.xyz);
	 vFogFactor = exp(-pow(dist * uDensity, uGradient));
	 vFogFactor = clamp(vFogFactor, 0.0, 1.0);
 #endif

	#HOOK_VERTEX_END

}

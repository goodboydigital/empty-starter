import * as PIXI from 'pixi.js';
import { Resource } from 'resource-loader';
import objParser from './objParser';

PIXI.geometryCache = {};

if (PIXI.Geometry)
{
    PIXI.Geometry.from = function (url)
    {
        return PIXI.geometryCache[url];
    };
}
else
{
    PIXI.Geometry.from = function (url)
    {
        return PIXI.geometryCache[url];
    };
}

export default function ()
{
    return function objLoader(resource, next)
    {
        // create a new texture if the data is an Image object
        if (resource.data && resource.type === Resource.TYPE.TEXT)
        {
            const geometry = objParser(resource.data);

            console.log('hey');

            resource.geometry = geometry;
            PIXI.geometryCache[resource.url] = geometry;
        }

        next();
    };
}

const path = require('path');

module.exports = {
	'app'       : path.join(__dirname, '../src/scripts/app'),
	'fido'      : path.join(__dirname, '../node_modules/fido.js/src'),
	'PIXI'      : path.join(__dirname, '../node_modules/pixi.js'),
	'pixi.js'   : path.join(__dirname, '../node_modules/pixi.js'),
	'gmi'       : path.join(__dirname, '../src/scripts/bbc/gmi'),
	'Const'     : path.join(__dirname, '../src/scripts/app/Const'),
	'ui'   		: path.join(__dirname, '../src/scripts/app/ui'),
	'buttons'   : path.join(__dirname, '../src/scripts/app/ui/buttons'),
	'overlay'   : path.join(__dirname, '../src/scripts/app/overlay'),
	'screens'   : path.join(__dirname, '../src/scripts/app/screens'),
	'system'    : path.join(__dirname, '../src/scripts/app/system'),
	'game'      : path.join(__dirname, '../src/scripts/game'),
	'control'   : path.join(__dirname, '../src/scripts/game/control'),
	'hud'       : path.join(__dirname, '../src/scripts/game/hud'),
	'odie'		: path.join(__dirname, '../node_modules/odie/lib'),
	'libs'		: path.join(__dirname, '../src/scripts/libs'),
}

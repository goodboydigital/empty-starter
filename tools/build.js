'use strict';

const makeBulldog = require('./makeBulldog.js');

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const serve = require('webpack-serve');

const config = require('./webpack.config');

const getPort = require('get-port');
const ip = require('ip').address();

var args = {};

process.argv.forEach(function (val, index, array) {
	args[val] = true;
});


async function startupDevelopment()
{
	// we preffer 8000!
	const preferedPort = 8000;

	const port = await getPort({port:preferedPort});

	const main = makeBulldog('./raw-assets', './dist/assets', './src/scripts/manifest.json');

	await main.run();

	await serve({ config, content:'./dist', host:ip, port})

	main.watch();
}

async function startupProduction()
{
	const main = makeBulldog('./raw-assets', './dist/assets', './src/scripts/manifest.json', true);

	await main.run();

	config.mode = 'production';

	const compiler = webpack(config);

	compiler.run(done=>{

		console.log("Build is complete! Have a lovely day.");
	});
}

if(args.production)
{
	startupProduction();
}
else
{
	startupDevelopment();
}



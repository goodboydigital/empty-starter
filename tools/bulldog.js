
var bulldog = require('bulldog');

var args = {};

process.argv.forEach(function (val, index, array) {
	args[val] = true;
});

const watch = args.watch;
const compress = args.compress;

const bulldogs = [];
const main = makeBulldog('./raw-assets', './dist/assets', './src/scripts/manifest.json');
bulldogs.push(main);

if(watch)
{
	bulldogs.forEach(b=>b.watch())
}
else
{
	bulldogs.forEach(b=>b.run())
}

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var alias = require('./alias')

module.exports = {

	entry: ['./src/scripts/index'],
	mode:'development',
	resolve:{
  		alias,
  	},
	module:{
		rules:[
			{
				test: /\.css$/,
				use: 'css-loader'
			},
			{
				test: /\.scss/,
				use:'css-loader'
			},
			{
				test: /\.(png|jpg|gif|woff|woff2)$/,
				use: 'url-loader?limit=8192'
			},
			{
				test: /\.(mp4|ogg|svg)$/,
				use: 'file-loader'
			},
			{
				test: /\.(glsl|frag|vert)$/,
				use: 'raw-loader'
			},
			// {
			// 	enforce: "pre",
		  //       test: /\.js$/,
		  //       exclude: /node_modules/,
		  //       loader: "eslint-loader",
			//     },
			{
				test: /\.js$/,
				include: /(src\/scripts|node_modules\/fido.js)/,
				use: {
					loader: 'babel-loader',
					options:{
				//		cacheDirectory: true,
						presets: [['env', {loose:true}]],
						plugins: [
							'transform-runtime',
							'add-module-exports',
						]
					}
				}
			}
		],
  	},
  	optimization: {
		minimizer: [
			new UglifyJSPlugin({
				uglifyOptions: {
					mangle:{
						keep_fnames:true
					},
					//mangle:false,
					compress:{
						warnings: true,
						drop_console:true
					}
				}
	    	})
		]
	},
  	output: {
    	filename: './main.js'
  	},

};


var bulldog = require('bulldog');

function makeBulldog(inputDir, ouputDir, manifest, compress)
{
	var assetMonitor = new bulldog.Bulldog(inputDir, ouputDir, true);

	assetMonitor.add( new bulldog.TexturePacker());
	assetMonitor.add( new bulldog.Gbo());
	assetMonitor.add( new bulldog.Audio());
	assetMonitor.add( new bulldog.MipImage());

	if(compress)
	{
		assetMonitor.add( new bulldog.TinyPNG());
		assetMonitor.add( new bulldog.Json());
	}

	assetMonitor.add( new bulldog.ManifestFido(manifest));
	assetMonitor.add( new bulldog.FileName());

	return assetMonitor;
}

module.exports = makeBulldog;
